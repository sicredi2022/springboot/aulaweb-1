package com.pucrs.sicred.aulaweb.controller;

import com.pucrs.sicred.aulaweb.model.User;
import com.pucrs.sicred.aulaweb.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 200 - SUCCESS
 * 404 - RESOURCE NOT FOUND
 * 400 - BAD REQUEST
 * 201 - CREATED
 * 401 - UNAUTHORIZED
 * 415 - UNSUPPORTED TYPE
 * 500 - SERVER ERROR
 */

@RestController
@CrossOrigin(origins = "http://localhost:8080/", maxAge = 3600)
@RequestMapping("/users")
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);
   @Autowired
    private UserService userService;
    @PostMapping
    public ResponseEntity<User> addUser(@Valid @RequestBody User user){
        logger.info("Iniciando adição do usuario");
        User userAdd = userService.add(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(userAdd);
    }
    @GetMapping
    public List<User> listAllUsers(){
        logger.debug("Debugging log");
        logger.info("Info log");
        logger.warn("Hey, This is a warning!");
        logger.error("Oops! We have an Error. OK");
        logger.fatal("Damn! Fatal error. Please fix me.");
        return userService.listUser();
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@RequestBody User user){
        Long id = user.getId() ;
        userService.delete(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> alterUser(@Valid @PathVariable Long id, @RequestBody User user){
        return ResponseEntity.ok(userService.alter(id, user));

    }

    @GetMapping("/{codigo}")
    public User listUserId(@PathVariable Long codigo){
        return ResponseEntity.ok(userService.validateUser(codigo)).getBody();
    }

    @GetMapping("/name/{name}")
    public String listUserName(@PathVariable String name){
        return "Lista  Usuário nome " + name;
    }

}
