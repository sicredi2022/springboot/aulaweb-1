package com.pucrs.sicred.aulaweb.controller;

import com.pucrs.sicred.aulaweb.model.Product;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {

    @GetMapping
    public String listAllProducts(){
        return "Lista todos os Produtos do sistema";
    }

    @GetMapping("/{codigo}")
    public String listProductId(@PathVariable Long codigo){
        return "Lista o Produto de código " + codigo;
    }

    @GetMapping("/name/{name}")
    public String listProductName(@PathVariable String name){
        return "Lista  Produto de nome " + name;
    }

    @GetMapping("/carro")
    public Product findProduct(){
        Product productFind = new Product();
        productFind.setId(1L);
        productFind.setNome("Carro");
        productFind.setMarca("GM");
        productFind.setModelo("Onyx");
        return productFind;
    }

    @PostMapping
    public String addProduct(){
        return "Adiciona Um Produto";
    }

    @PutMapping
    public String alterProduct(){
        return "Altera Um Produto";
    }

    @DeleteMapping
    public String deleteProduct(){
        return "Exclui um Produto";
    }

}
