package com.pucrs.sicred.aulaweb.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Erro {

    private String msgUsuario;
    private String msgDesenvolvedor;
}
