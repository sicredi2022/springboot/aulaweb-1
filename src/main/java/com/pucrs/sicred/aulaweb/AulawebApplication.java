package com.pucrs.sicred.aulaweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "com.pucrs.sicred.aulaweb.model")
@EnableJpaRepositories(basePackages = "com.pucrs.sicred.aulaweb.repository")
@ComponentScan(basePackages = {"com.pucrs.sicred.aulaweb.service","com.pucrs.sicred.aulaweb.controller",
		"com.pucrs.sicred.aulaweb.exception", "com.pucrs.sicred.aulaweb.config.security"})
public class AulawebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AulawebApplication.class, args);
	}

}
