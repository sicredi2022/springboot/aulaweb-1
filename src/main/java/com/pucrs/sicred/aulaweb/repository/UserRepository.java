package com.pucrs.sicred.aulaweb.repository;

import com.pucrs.sicred.aulaweb.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
