package com.pucrs.sicred.aulaweb.model;


import com.pucrs.sicred.aulaweb.exception.Constantes;
import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "usuario")
public class User {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Nome " + Constantes.OBRIGATORIO + " ")
    @Length(min = 3, max = 60, message = "Nome tem que ter entre 3 e 60 caracteres")
    private String name;
    @NotBlank(message = "E-Mail " + Constantes.OBRIGATORIO + " ")
    @Length(min = 10, max = 60, message = "E-Mail")
  // @Pattern(regexp = "@")
    private String email;

    public User(Integer id, String name, String email) {
        this.name = name;
        this.email = email;
    }

    public User() {

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
