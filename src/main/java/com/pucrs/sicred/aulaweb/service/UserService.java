package com.pucrs.sicred.aulaweb.service;

import com.pucrs.sicred.aulaweb.model.User;
import com.pucrs.sicred.aulaweb.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User add(User user){
        return userRepository.save(user);
    }

    public void delete(Long id){
        userRepository.deleteById(id);
    }

    public List<User> listUser(){
        return userRepository.findAll();
    }

    public User alter(Long id, User user) {
        User userAlter = validateUser(id);
        BeanUtils.copyProperties(user, userAlter, "id");
        return userRepository.save(userAlter);
    }

    public User validateUser(Long id){
        Optional<User> user = userRepository.findById(id);
        if(user.isEmpty()){
            throw new EmptyResultDataAccessException(1);
        }
        return user.get();
    }

}
